FROM openjdk:8

EXPOSE 8080/tcp

RUN mkdir /build
COPY . /build/
RUN cd /build && ./gradlew clean build && mv build/libs/opening-hours*.jar ../opening-hours.jar

ENTRYPOINT [ \
"java", \
"-jar", \
"/opening-hours.jar"]