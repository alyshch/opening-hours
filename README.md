# Opening hours

### How to build
You can either build docker image: `docker build -t <name> .` and run it 
`docker run -p 8080:8080 <name>`. Or, if you have Java 1.8 installed, you can run
```
./gradlew build
cp ./build/libs/opening-hours*.jar opening-hours.jar
java -jar opening-hours.jar
```
### Usage
Once running, app will listen to port 8080. 
To receive a opening hours as text, `POST` to endpoint `/v1/opening-hours/as-text` 
with a JSON body of aforementioned form 
```
{
    <dayofweek>: <opening hours>
    <dayofweek>: <opening hours>
    ...
}
```
### Data format for opening hours
Regarding the second questing of the deliverable: whether or not data format
is a good fit to use depends what we need to do with the data. If all we need
to do after storing opening hours is respond with textual representation, then
list of intervals like `<start, end>` for each day will be more convenient.
If we need, for example, check if a restaurant is open for some particular time,
both formats are similar in use.