package com.github.bouncenow.openinghours.controller

import com.github.bouncenow.openinghours.asText
import com.github.bouncenow.openinghours.model.BadRequestException
import com.github.bouncenow.openinghours.model.OpeningHours
import com.github.bouncenow.openinghours.model.OpeningHoursAsTextResponse
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.lang.IllegalArgumentException

@RestController
@RequestMapping("v1/opening-hours/as-text")
class OpeningHoursController {

    @PostMapping
    fun asText(@RequestBody openingHours: OpeningHours) =
        try {
            OpeningHoursAsTextResponse(openingHours.asText())
        } catch (e: IllegalArgumentException) {
            log.error("Error during opening hours to text conversion: $e")
            throw BadRequestException(e.message)
        }

    companion object {
        private val log = LoggerFactory.getLogger(OpeningHoursController::class.java)
    }

}