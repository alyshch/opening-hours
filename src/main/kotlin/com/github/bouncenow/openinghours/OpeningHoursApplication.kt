package com.github.bouncenow.openinghours

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class OpeningHoursApplication

fun main(args: Array<String>) {
    runApplication<OpeningHoursApplication>(*args)
}
