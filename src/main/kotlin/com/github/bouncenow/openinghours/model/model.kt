package com.github.bouncenow.openinghours.model

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty

enum class DayOfWeek {
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY,
    SUNDAY;

    companion object {
        @JsonCreator
        @JvmStatic
        fun fromString(stringValue: String?) = stringValue?.let { valueOf(it.toUpperCase()) }
    }
}

data class OpeningHoursEvent(val type: Type, @JsonProperty("value") val time: Int) {
    enum class Type {
        OPEN, CLOSE;

        companion object {
            @JsonCreator
            @JvmStatic
            fun fromString(stringValue: String?) = stringValue?.let { valueOf(it.toUpperCase()) }
        }
    }
}

typealias OpeningHours = Map<DayOfWeek, List<OpeningHoursEvent>>

data class OpeningHoursAsTextResponse(val response: String)