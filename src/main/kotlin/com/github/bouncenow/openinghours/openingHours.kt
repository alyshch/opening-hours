package com.github.bouncenow.openinghours

import com.github.bouncenow.openinghours.model.DayOfWeek
import com.github.bouncenow.openinghours.model.OpeningHours
import com.github.bouncenow.openinghours.model.OpeningHoursEvent


fun OpeningHours.asText(): String {
    val events = asListOfEvents()
    if (events.isEmpty()) {
        return intervalsByDayAsText(emptyMap())
    }
    require(events.size % 2 == 0) {
        "Not for every 'open' event there's a 'close' event"
    }

    val startFrom = events.indexOfFirst { it.event.type == OpeningHoursEvent.Type.OPEN }
    require(startFrom >= 0) {
        "Not a single 'open' event"
    }

    val intervalsByDay = hashMapOf<DayOfWeek, MutableList<Interval>>()

    var cur = startFrom
    do {
        val curEvent = events[cur]
        val nextEvent = events[(cur + 1) % events.size]
        val intervals = intervalsByDay.getOrPut(curEvent.day) { ArrayList() }
        intervals.add(validateAndGetInterval(curEvent, nextEvent, intervals.lastOrNull()))
        cur = (cur + 2) % events.size
    } while (cur != startFrom)

    return intervalsByDayAsText(intervalsByDay)
}

private fun intervalsByDayAsText(intervalsByDay: Map<DayOfWeek, List<Interval>>) =
    DayOfWeek.values().asSequence()
        .joinToString(separator = "\n") { day ->
            val intervalsJoined = intervalsByDay[day]?.asSequence()
                ?.joinToString(separator = ", ") { (start, end) ->
                    "${secondsAs12HoursTimeString(start)} - ${secondsAs12HoursTimeString(end)}"
                }
                ?: "Closed"
            "${day.format()}: $intervalsJoined"
        }

fun DayOfWeek.format() = "${name[0]}${name.substring(1).toLowerCase()}"

fun secondsAs12HoursTimeString(seconds: Int): String {
    val hours = seconds / SECONDS_IN_HOUR
    val minutes = (seconds % SECONDS_IN_HOUR) / SECONDS_IN_MINUTE
    val remainder = hours % HALF_DAY
    val (hours1To12Value, suffix) = if (remainder != 0) {
        remainder to if (hours > HALF_DAY) PM else AM
    } else {
        HALF_DAY to if (hours == 0) PM else AM
    }
    return "${if (minutes != 0) "$hours1To12Value.$minutes" else hours1To12Value.toString()} $suffix"
}

private fun validateAndGetInterval(start: EventAtDay, end: EventAtDay, previousInterval: Interval?): Interval {
    start.event.validateType(OpeningHoursEvent.Type.OPEN)
    end.event.validateType(OpeningHoursEvent.Type.CLOSE)
    if (start.day == end.day) {
        require(start.event.time < end.event.time && start.indexAtDay < end.indexAtDay) {
            "Close event before open at the same day"
        }
    } else {
        require(
            (end.day.ordinal - start.day.ordinal == 1) ||
                    (end.day == DayOfWeek.MONDAY && start.day == DayOfWeek.SUNDAY)
        ) {
            "Illegal days for opening hours interval ${start.day} - ${end.day}"
        }
    }

    start.event.validateTime()
    end.event.validateTime()

    require(previousInterval == null || previousInterval.second < start.event.time) {
        "Intervals at ${start.day} overlap"
    }

    return start.event.time to end.event.time
}

private const val MAX_SECONDS = 86400
private const val SECONDS_IN_MINUTE = 60
private const val SECONDS_IN_HOUR = 60 * 60
private const val HALF_DAY = 12
private const val AM = "AM"
private const val PM = "PM"

private fun OpeningHoursEvent.validateType(expected: OpeningHoursEvent.Type) = require(type == expected) {
    "Two '$this' events back to back"
}


private fun OpeningHoursEvent.validateTime() = require(time in 0 until MAX_SECONDS) {
    "Invalid time value: $time"
}

private fun OpeningHours.asListOfEvents() =
    DayOfWeek.values().asSequence()
        .flatMap { day ->
            get(day)?.asSequence().orEmpty()
                .sortedBy { it.time }
                .mapIndexed { i, events -> EventAtDay(events, day, i) }
        }
        .toList()

private data class EventAtDay(val event: OpeningHoursEvent, val day: DayOfWeek, val indexAtDay: Int)

private typealias Interval = Pair<Int, Int>