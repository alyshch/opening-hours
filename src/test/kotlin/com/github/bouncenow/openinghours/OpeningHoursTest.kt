package com.github.bouncenow.openinghours

import com.github.bouncenow.openinghours.model.DayOfWeek
import com.github.bouncenow.openinghours.model.DayOfWeek.*
import com.github.bouncenow.openinghours.model.OpeningHoursEvent
import com.github.bouncenow.openinghours.model.OpeningHoursEvent.Type.CLOSE
import com.github.bouncenow.openinghours.model.OpeningHoursEvent.Type.OPEN
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class OpeningHoursTest {

    @Test
    fun `test simple opening hours`() {
        assertOpeningHours(
            expectedTextFor(
                MONDAY to "8 AM - 6 PM",
                TUESDAY to "10 AM - 10 PM",
                THURSDAY to "12 AM - 7 PM, 8 PM - 11 PM",
                FRIDAY to "1 AM - 8 AM",
                SATURDAY to "3 PM - 11 PM"
            ),
            MONDAY to listOf(OPEN at 8.AM, CLOSE at 6.PM),
            TUESDAY to listOf(OPEN at 10.AM, CLOSE at 10.PM),
            THURSDAY to listOf(OPEN at 12.AM, CLOSE at 7.PM, OPEN at 8.PM, CLOSE at 11.PM),
            FRIDAY to listOf(OPEN at 1.AM, CLOSE at 8.AM),
            SATURDAY to listOf(OPEN at 3.PM, CLOSE at 11.PM)
        )
    }

    @Test
    fun `test with closings on the next day`() {
        assertOpeningHours(
            expectedTextFor(
                MONDAY to "8 PM - 1 AM",
                TUESDAY to "9 PM - 3 AM",
                WEDNESDAY to "7 PM - 9 PM, 11 PM - 5 AM",
                THURSDAY to "1 PM - 1 AM",
                FRIDAY to "3 PM - 1 AM",
                SATURDAY to "8 PM - 8 AM",
                SUNDAY to "9 PM - 3 AM"
            ),
            MONDAY to listOf(CLOSE at 3.AM, OPEN at 8.PM),
            TUESDAY to listOf(CLOSE at 1.AM, OPEN at 9.PM),
            WEDNESDAY to listOf(CLOSE at 3.AM, OPEN at 7.PM, CLOSE at 9.PM, OPEN at 11.PM),
            THURSDAY to listOf(CLOSE at 5.AM, OPEN at 1.PM),
            FRIDAY to listOf(CLOSE at 1.AM, OPEN at 3.PM),
            SATURDAY to listOf(CLOSE at 1.AM, OPEN at 8.PM),
            SUNDAY to listOf(CLOSE at 8.AM, OPEN at 9.PM)
        )
    }

    @Test
    fun `test empty`() {
        assertOpeningHours(
            expectedTextFor()
        )
    }

    @Test
    fun `should throw when close before open`() {
        assertInvalidArgument(
            MONDAY to listOf(CLOSE at 3.AM, OPEN at 1.PM)
        )
    }

    @Test
    fun `should throw when same events back to back`() {
        assertInvalidArgument(
            TUESDAY to listOf(OPEN at 10.AM, CLOSE at 8.PM, OPEN at 11.PM),
            WEDNESDAY to listOf(OPEN at 10.AM, CLOSE at 1.PM)
        )
        assertInvalidArgument(
            TUESDAY to listOf(OPEN at 10.AM, CLOSE at 8.PM),
            WEDNESDAY to listOf(CLOSE at 10.AM, OPEN at 1.PM),
            THURSDAY to listOf(CLOSE at 1.AM, OPEN at 1.PM, CLOSE at 2.PM)
        )
    }

    @Test
    fun `should throw when invalid time`() {
        assertInvalidArgument(
            MONDAY to listOf(OPEN at 10.AM, CLOSE at 86400)
        )
    }

    @Test
    fun `should throw when open spans for multiple days`() {
        assertInvalidArgument(
            MONDAY to listOf(OPEN at 10.AM),
            WEDNESDAY to listOf(CLOSE at 1.PM, OPEN at 3.PM, CLOSE at 4.PM)
        )
    }

    @Test
    fun `should throw when intervals overlap`() {
        assertInvalidArgument(
            MONDAY to listOf(OPEN at 1.PM, CLOSE at 5.PM, OPEN at 4.PM, CLOSE at 9.PM)
        )
        assertInvalidArgument(
            FRIDAY to listOf(OPEN at 6.PM),
            SATURDAY to listOf(CLOSE at 10.AM, OPEN at 9.AM, CLOSE at 5.PM)
        )
    }

    private fun assertOpeningHours(expectedText: String, vararg intervals: Pair<DayOfWeek, List<OpeningHoursEvent>>) {
        assertEquals(expectedText, hashMapOf(*intervals).asText())
    }

    private fun assertInvalidArgument(vararg intervals: Pair<DayOfWeek, List<OpeningHoursEvent>>) {
        assertThrows<IllegalArgumentException> {
            hashMapOf(*intervals).asText()
        }
    }

    private fun expectedTextFor(vararg openingHoursTextByDays: Pair<DayOfWeek, String>): String {
        val asMap = hashMapOf(*openingHoursTextByDays)
        return DayOfWeek.values().asSequence()
            .joinToString(separator = "\n") { day ->
                "${day.format()}: ${asMap[day] ?: "Closed"}"
            }
    }

    val Int.PM: Int
        get() = (this + 12) * 3600

    val Int.AM: Int
        get() = this * 3600

    private infix fun OpeningHoursEvent.Type.at(time: Int) = OpeningHoursEvent(this, time)

}