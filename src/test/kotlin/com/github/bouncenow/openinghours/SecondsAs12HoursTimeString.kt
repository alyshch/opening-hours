package com.github.bouncenow.openinghours

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test


class SecondsAs12HoursTimeString {

    @Test
    fun `test simple`() {
        assertEquals(
            "1 PM",
            secondsAs12HoursTimeString(13 * 3600)
        )
        assertEquals(
            "2 AM",
            secondsAs12HoursTimeString(2 * 3600)
        )
    }

    @Test
    fun `test midnight noon`() {
        assertEquals(
            "12 AM",
            secondsAs12HoursTimeString(12 * 3600)
        )

        assertEquals(
            "12 PM",
            secondsAs12HoursTimeString(0)
        )
    }

    @Test
    fun `test minutes`() {
        assertEquals(
            "2.35 AM",
            secondsAs12HoursTimeString(2 * 3600 + 35 * 60)
        )
        assertEquals(
            "4.52 PM",
            secondsAs12HoursTimeString(16 * 3600 + 52 * 60 + 45)
        )
    }

}